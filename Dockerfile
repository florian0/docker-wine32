FROM ubuntu:18.04

RUN dpkg --add-architecture i386
RUN apt-get update && apt-get install -y wine32 winetricks xvfb winbind
